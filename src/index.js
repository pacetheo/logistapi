require('babel-polyfill');

import http from 'http'
import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'
// libs
import middleware from './middleware'
import api from './api'
import {connectMongo, initMongo} from "./lib/mongoUtils"

// environment
import config from './config.json'

let app = express()
app.server = http.createServer(app)

	
// logger
app.use(morgan('dev'))

// 3rd party middleware
app.use(cors({
	exposedHeaders: config.corsHeaders 
}))

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())


connectMongo("admin")
.then(db =>  initMongo(db))
.then(db =>{
	
	// internal middleware
	app.use(middleware({ config, db }))

	// api router
	app.use('/api', api({ config, db }))

	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`)
	})
})
.catch(err =>{
	console.log("ERROR launching server")
	throw err
})

export default app
