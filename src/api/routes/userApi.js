
import User from '../../models/users'



const userApi = (router) =>
    // adding the /user route to our /api router
    router.route('/user')
        // retrieve all users from the database
        .get((req, res) => {
            // looks at our Comment Schema
            console.log(req.query)
            const researchParam = { login: req.query.login, _id: req.query._id, isAdmin: req.query.isAdmin }
            Object.keys(researchParam).forEach(key => { if (typeof researchParam[key] === 'undefined') delete researchParam[key] })
            console.log(researchParam)
            User.find(researchParam)
                .then((users) => {
                    res.send(users)
                })
                .catch(err => {
                    console.log("[User API-get] problem with the request:")
                    console.log(err)
                    res.send(err)
                })
        })
        // post new comment to the database
        .post((req, res) => {
            if (!(req.body.login && req.body.password)) {
                res.json({ message: 'Body empty  or not complete', body: req.body })
                console.log('[userApi-Post]insertion impossible body uncomplete :' + JSON.stringify(req.body))
            } else {
                const user = new User()
                // body parser lets us use the req.body
                user.login = req.body.login
                user.password = req.body.password
                user.isAdmin = Boolean(req.body.isAdmin)
                user.description = (req.body.description)?req.body.description:null
                user.contact = (req.body.contact)?req.body.contact:null
                user.fiches = []

                user.save((err) => {
                    if (err) {
                        res.send(err)
                    }
                    else {
                        console.log("[API user] post succeded" + user)
                        res.json({ message: 'user successfully added!' })
                    }
                })
            }
        })
        .delete((req, res) => {
            let rm
            if (req.query.all) {
                console.log("[user]delete all")
                rm = User.deleteMany({})
            } else {
                const query = {}
                if (req.query.login !== undefined) query.login = req.query.login
                if (req.query.isAdmin !== undefined) query.isAdmin = req.query.isAdmin
                if (req.query._id !== undefined) query._id = req.query._id
                console.log(query)
                if(JSON.stringify(query) === JSON.stringify({})) {
                    console.log(`query empty  or not complete query : ${JSON.stringify(req.query)} query: ${JSON.stringify(req.query)}`)
                    res.status(406).json({ message: 'query empty  or not complete', query: req.query })
                } else {
                    console.log(`successful deletion : ${JSON.stringify(req.query)} query: ${JSON.stringify(req.query)}`)
                    rm = User.deleteMany(query)
            rm.then(result => {
                console.log('[delete consumption]: ' + JSON.stringify(req.query) + ' ==> ' + result)
                res.json({ result })
                })
            }
            }
        })
        .put((req, res) => {
            if (!(req.query.hasOwnProperty('_id'))) {
                res.status(406).json({ message: 'Body empty  or not complete', body: req.query })
                console.log('[userApi-Put]insertion impossible query uncomplete :' + JSON.stringify(req.query))
            } else {
                User.findByIdAndUpdate(req.query._id, req.body, { new: true})
                    .then((result) => {
                        console.log("[userApi] put succedded " + result + " body was " + JSON.stringify(req.body))
                        res.status(200).json({ success: true, result })
                    })
                    .catch(err => {
                        console.log('[userApi - put] error' + err)
                        res.status(err.status).send({ sucess: false, err: err.toString(), context: req.body })
                    })
            }
        })
module.exports = userApi
