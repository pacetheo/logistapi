
import Fiche from '../../models/fiches'

const compareDates = (d1,d2) => {
    const d1_v = d1.getUTCMonth()+"/"+d1.getUTCDate()+"/"+d1.getUTCFullYear()
    const d2_v = d2.getUTCMonth()+"/"+d2.getUTCDate()+"/"+d2.getUTCFullYear()
    console.log(d1_v + "    "+d2_v)
    return d1_v === d2_v 

}

const ficheApi = (router) => {
    router.route('/fiche/date')
        .get((req,res)=>{
            console.log(`[get fiche datz] research param ${req.query}`)
            let dateValue, dateType
            if(req.query.dateReception){
                dateType = "dateReception"
                dateValue = new Date(req.query.dateReception)
            } else if (req.query.dateRenvoie) {
                dateType = "dateRenvoie"
                dateValue = new Date(req.query.dateRenvoie)
            } else {
                res.status(406).json({message: "missing date to select", body: req.query})
            }
            console.log(dateValue)
            Fiche.find({})
            .then((allFiches) => 
                allFiches.filter(fiche => compareDates(fiche[dateType], dateValue))
            ).then(filtered =>{
                console.log(filtered)
                res.status(200).json({filtered})
            })
        })
    // adding the /fiche route to our /api router
    router.route('/fiche')
        // retrieve all fiches from the database
        .get((req, res) => {
            // retrives one fiche from database
            console.log(req.query)
            const researchParam = { 
                nom: req.query.nom, 
                _id: req.query._id, 
                loginRenvoie: req.query.loginRenvoie, 
                loginReception: req.query.loginReception, 
                fournisseur: req.query.fournisseur,
                emplaceRangement: req.query.emplaceRangement
             }
            Object.keys(researchParam).forEach(key => { if (typeof researchParam[key] === 'undefined') delete researchParam[key] })
            console.log(`[get fiche] research param ${req.query}`)
            Fiche.find(researchParam)
                .then((fiches) => {
                    res.send(fiches)
                })
                .catch(err => {
                    console.log("[Fiche API-get] problem with the request:")
                    console.log(err)
                    res.send(err)
                })
        })
        // post new fiche to the database
        .post((req, res) => {
            if (!(req.body.loginReception && req.body.fournisseur 
                && req.body.photoReception && req.body.nom)) {
                res.json({ message: 'Body empty  or not complete', body: req.body })
                console.log('[ficheApi-Post]insertion impossible body uncomplete :' + JSON.stringify(req.body))
            } else {
                const fiche = new Fiche()
                // body parser lets us use the req.body
                fiche.photoReception = req.body.photoReception
                fiche.nom = req.body.nom
                fiche.loginReception = req.body.loginReception
                fiche.fournisseur = req.body.fournisseur
                fiche.dateReception = new Date(Date())
                fiche.quantiteReception = req.body.quantiteReception
                fiche.emplacementRangement = req.body.emplacementRangement
                fiche.commentaireReception = req.body.commentaireReception
                fiche.renvoie = false

                fiche.save((err) => {
                    if (err) {
                        res.send(err)
                    }
                    else {
                        console.log("[API fiche] post succeded" + JSON.stringify(fiche))
                        res.status(200).json({ message: 'fiche successfully added!' })
                    }
                })
            }
        })
        .delete((req, res) => {
            let rm
            if (req.query.all) {
                console.log("[fiche]delete all")
                rm = Fiche.deleteMany({})
            } else {
                const query = {}
                if (req.query._id !== undefined) query._id = req.query._id
                console.log(query)
                if(JSON.stringify(query) === JSON.stringify({})) {
                    console.log(`query empty  or not complete query : ${JSON.stringify(req.query)} query: ${JSON.stringify(req.query)}`)
                    res.status(406).json({ message: 'query empty  or not complete', query: req.query })
                } else {
                    console.log(`successful deletion : ${JSON.stringify(req.query)} query: ${JSON.stringify(req.query)}`)
                    rm = Fiche.deleteMany(query)
            rm.then(result => {
                console.log('[delete fiche]: ' + JSON.stringify(req.query) + ' ==> ' + result)
                res.json({ result })
                })
            }
            }
        })
        .put((req, res) => {
            if (!(req.query.hasOwnProperty('_id'))) {
                console.log('[FicheApi-Put]insertion impossible query uncomplete :' + JSON.stringify(req.query))                
                res.status(406).json({ message: 'Body empty  or not complete', query: req.query })
            } else {
                if(req.body.renvoie){
                    if(!(req.body.photoRenvoie && req.body.loginRenvoie)){
                        console.log(`body empty  or not complete body : ${JSON.stringify(req.body)} body: ${JSON.stringify(req.body)}`)
                        res.status(406).json({ message: 'body empty  or not complete', body: req.body })                    
                    }
                } 
                    console.log(req.query, req.body)
                    Fiche.findByIdAndUpdate(req.query._id, req.body, { new: true})
                    .then((result) => {
                        console.log("[FicheApi] put succedded " + result + " body was " + JSON.stringify(req.body))
                        res.status(200).json({ success: true, result })
                    })
                    .catch(err => {
                        console.log('[FicheApi - put] error' + err)
                        res.status(err.status).send({ sucess: false, err: err.toString(), context: req.body })
                    })
                
            }
        })
    return router
    }
module.exports = ficheApi
