const multer = require("multer")

const config = require("../../config.json")
const md5 = require('md5');

const fs = require("fs")
const path = require("path")
import { promisify } from 'util';

const readdir = promisify(fs.readdir)
const readfile = promisify(fs.readFile)
const rename = promisify(fs.rename)
const unlink = promisify(fs.unlink)

const handleError = (err, res) => {
    console.log(err)
    res.status(500).contentType("text/plain").end("Oops! Something went wrong!")
}

const upload = multer({
    dest: config.storage
})
const uploadDir = path.join(path.dirname(require.main.filename), "../uploads/")
fs.readdir(uploadDir, (err, files) => {
    if (err) throw err;
    for (const file of files) {
        fs.unlink(path.join(uploadDir, file), err => {
            if (err) throw err;
        });
    }
})

//TODO: crash when MD5 not equal
const extensions = [".png", ".jpg", ".jpeg", ".img"]
const pictureApi = (router) => {
    // adding the /photo route to our /api router
    router.route('/picture')
        .post(
            upload.single("picture" /* name attribute of <file> element in your form */),
            async (req, res) => {
                const tempPath = req.file.path
                const fileMd5 = req.body.md5

                let buf = await readfile(tempPath)
                console.log(fileMd5)
                console.log(md5(buf))

                if (md5(buf) != fileMd5) {
                    
                    try {
                        await unlink(tempPath)
                    } catch (err) {
                        return handleError(err, res);
                    }

                    return res.status(415).contentType("text/plain").end(`MD5 not corresponding, please resend`)
                }
                const login = req.body.login
                const current_date = new Date(Date()).getTime()
                const imageName = login + "-" + current_date + "-" + req.file.originalname
                const targetPath = uploadDir + imageName
                const extension = path.extname(req.file.originalname).toLowerCase()

                if (extensions.includes(extension)) {
                    try {
                        await rename(tempPath, targetPath)
                    }
                    catch (err) {
                        return handleError(err, res)
                    }
                    res.status(200)
                        .json({ file: targetPath, login: login, originalname: req.file.originalname })
                } else {
                    try {
                        await unlink(tempPath)
                    } catch (err) {
                        return handleError(err, res);
                    }
                    res
                        .status(403)
                        .contentType("text/plain")
                        .end(`Only ${extensions}, files are allowed!`)
                }
            }
        )

        .delete((req, res) => {
            let deleted = false
            if (!req.query.file) {
                console.log(`query empty  or not complete query : ${JSON.stringify(req.query)} query: ${JSON.stringify(req.query)}`)
                res.status(406).json({ message: 'query empty  or not complete', query: req.query })
            } else {
                readdir(uploadDir)
                    .then((files) => {
                        files.map(file => {
                            if (file == req.query.file) {
                                unlink(path.join(uploadDir, file))
                                console.log(`successful deletion : ${JSON.stringify(req.query)} query: ${JSON.stringify(req.query)}`)
                                deleted = true
                            }
                        })
                    })
                    .then(() => {
                        if (!deleted) {
                            console.log(`deleted : ${deleted}`)
                            res.status(404).json({ message: 'could not find file', query: req.query })
                        } else {
                            res.status(200).json({ message: "success", file: req.query.file })
                        }
                    })

            }
        })
    return router
}
module.exports = pictureApi
