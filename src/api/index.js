import { version } from '../../package.json';
import { Router } from 'express';
const userApi = require('./routes/userApi')
const ficheApi = require('./routes/ficheApi')
const pictureApi = require('./routes/pictureApi')

export default ({ config, db }) => {
	let api = Router();

	// mount the facets resource

	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version });
	})
	userApi(api)
	ficheApi(api)
	pictureApi(api)
	return api;
}
