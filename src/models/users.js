const mongoose = require("mongoose")
const Schema = mongoose.Schema
mongoose.Promise = global.Promise
// Create schema and model

const userSchema = new Schema({
    login: String,
    password: String,
    description: String,
    fiches: Array,
    contact: String,
    isAdmin: Boolean
})
 
const User = mongoose.model('user',userSchema)

module.exports = User