const mongoose = require("mongoose")
const Schema = mongoose.Schema

// Create schema and model


const ficheSchema = new Schema({
    photoReception: String, //path to server
    photoRenvoie: String, //path to server
    nom: String,
    loginReception: String,
    loginRenvoie: String,
    fournisseur: String,
    dateReception: Date,
    dateRenvoie: Date,
    quantiteReception: Number,
    quantiteRenvoie: Number,
    emplacementRangement: String,
    commentaireReception: String,
    commentaireRenvoie: String,
    renvoie: Boolean

})
 
const Fiche = mongoose.model('fiche',ficheSchema)

module.exports = Fiche