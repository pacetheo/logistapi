const mongoose = require('mongoose')

const config = require('../config.json')
const User = require('../models/users')
const Fiche = require('../models/fiches')

const aymeric = new User({
    login: "aymeric",
    password: "pw",
    description: "blond",
    contact: "bg@if.com",
    isAdmin: true
})
const pepito = new User({
    login: "pepito",
    password: "pw",
    description: "un peu blond",
    contact: "bg@if.com",
    isAdmin: false
})
const  julon = new User({
        login: "julon",
        password: "pw",
        description: "brune",
        contact: "bgette@if.com",
        isAdmin: true
    })

const hippo = new User({
    login: "hippo",
    password: "pw",
    description: "pas blond",
    contact: "bg@if.com",
    isAdmin: false})

const truc1 = new Fiche({
    dateReception: new Date(Date()),
    photoReception: "fake/path1",
    nom: "truc1",
    loginReception: "hippo1",
    fournisseur: "dequerre1",
    quantiteReception: 1,
    emplacementRangement: "DTC1",
    commentaireReception: "D'EQQQQQQQQUURE1",
    })

const truc2 = new Fiche({
    dateReception: new Date(Date()),
    photoReception: "fake/path2",
    nom: "truc2",
    loginReception: "hippo2",
    fournisseur: "dequerre2",
    quantiteReception: 2,
    emplacementRangement: "DTC2",
    commentaireReception: "D'EQQQQQQQQUURE2",
    })

const truc3 = new Fiche({
    dateReception: new Date(Date()),
    photoReception: "fake/path3",
    nom: "truc3",
    loginReception: "hippo3",
    fournisseur: "dequerre3",
    quantiteReception: 3,
    emplacementRangement: "DTC3",
    commentaireReception: "D'EQQQQQQQQUURE3",
    })
const connectMongo = (collection) => {
    const mongoUri = `mongodb://${config.dbRootUsername}:${config.dbRootPassword}@${config.urlMongodb}:${config.mongoPort}/${collection}`
    console.log(`attempting to connect with ${mongoUri}` )
mongoose.connection.once('open', ()=>
    console.log("Connexion to mongoose succeded"))

mongoose.connection.on('error', err => {
    console.log("connexion to mongose error")
    console.log(err)
})

return mongoose.connect(mongoUri)
}

const initMongo = (db) => {    
    return mongoose.connection.collections.users.deleteMany()
    .then(()=> mongoose.connection.collections.fiches.deleteMany())
    .then((rslt) =>
        aymeric.save())
    .then(() => 
        pepito.save())
    .then(() => 
        julon.save())
    .then(() => 
        hippo.save())
    .then( rslt => User.find({}))
    .then(rslt => {
            console.log(rslt)
            return db
        })
    .then(() => 
        truc1.save())
    .then(() => 
        truc2.save())
    .then(() => 
        truc3.save())
    .then( rslt => Fiche.find({}))
    .then(rslt => {
            console.log(rslt)
            return db
        })
    }


module.exports = {connectMongo, initMongo}