const mocha = require("mocha")
const assert = require("assert")
const mongoose = require("mongoose")

const User = require("../src/models/users")

const connectMongo = require("../src/lib/mongoUtils")

// before any test, connect mongo
before((done) => {
    connectMongo("admin").then(x => done())
})
// before each test, drop the collections to run the test in isolation



describe("test on mongo",() => {
    beforeEach((done) =>{
        // drop collection
        mongoose.connection.collections.users.drop().then(x =>{ 
            let aymeric = new User({
                login: "aymeric",
                password: "pw",
                description: "blond",
                contact: "bg@if.com",
                isAdmin: true
            })
        return aymeric.save()}).then(x =>{ 
            const userEx = new User({
                login: "pepito",
                password: "pw",
                description: "un peu blond",
                contact: "bg@if.com",
                isAdmin: false
            })
            return userEx.save()}).then(x =>{ 
                const userEx = new User({
                    login: "julon",
                    password: "pw",
                    description: "brune",
                    contact: "bgette@if.com",
                    isAdmin: true
                })
                return userEx.save()})
        .then( ()=>{ 
            done()
        })
    })
    it("save a record to the db", (done)=> {
        const userEx = new User({
            login: "hippo",
            password: "pw",
            description: "brun",
            contact: "bg@if.com",
            isAdmin: false
        })
        userEx.save().then( ()=>{
            assert(userEx.isNew === false)
            done()
        })
    })
})

describe("finding on mongo",() => {
    let aymeric = new User({
        login: "aymeric",
        password: "pw",
        description: "blond",
        contact: "bg@if.com",
        isAdmin: true
    })
    beforeEach((done) => {
        // drop collection
        aymeric.save()
        .then(x =>{ 
            const userEx = new User({
                login: "pepito",
                password: "pw",
                description: "un peu blond",
                contact: "bg@if.com",
                isAdmin: false
            })
            return userEx.save()})

        .then(x =>{ 
            const userEx = new User({
                login: "julon",
                password: "pw",
                description: "brune",
                contact: "bgette@if.com",
                isAdmin: true
            })
            return userEx.save()})
        .then( x =>{ 
            done()
        })
    })

    it("finding by id", (done) =>{
        User.findOne({_id: aymeric._id}).then((rslt) =>{
            assert(rslt.login === 'aymeric')
            done()
        })
    })
    it("finding admins",(done)=>{
        User.find({isAdmin:true}).then((rslt) =>{
            assert(rslt.length >= 1)
            done()
        })
    })
    it("search by aymerics", (done) =>{
        User.find().then(x => console.log(x)).then(()=>{
        User.findOne({login:"aymeric"}).then(rslt =>{
            assert(aymeric._id.toString() === rslt._id.toString() )
            done()
        })
    })
    })
})  